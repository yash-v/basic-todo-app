import { useState } from "react";
import "./App.css";

// components
import Form from "./components/Form";
import TodoList from "./components/TodoList";

function App() {

  const [input, setInput] = useState("")
  const [todos, setTodos] = useState([])
  
  return (
    <div className="App">
      <header>
        <h1>Yash's Todo List</h1>
      </header>

      <Form inputText={input} setInputText={setInput} todos={todos} setTodos={setTodos} />
      <TodoList />
    </div>
  );
}

export default App;
